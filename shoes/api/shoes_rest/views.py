from django.shortcuts import render
from .models import Pair, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Pair, BinVO

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]



class PairEncoder(ModelEncoder):
    model = Pair
    properties = [
        "id",
        "manufacturer",
        "model",
        "color",
        "photo_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_pairs(request):
    if request.method == "GET":
        pairs = Pair.objects.all()
        return JsonResponse(
            {"pairs": pairs},
            encoder=PairEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)


        try:
                bin_id = content["bin"]
                bin_href = f"/api/bins/{bin_id}/"
                bin = BinVO.objects.get(import_href=bin_href)
                content["bin"] = bin

        except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid pair id"},
                    status=400,
                )

        pairs = Pair.objects.create(**content)
        return JsonResponse(
                pairs,
                encoder=PairEncoder,
                safe=False,
            )




@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_pair(request, id):
    if request.method == "GET":
        pairs = Pair.objects.get(id=id)
        return JsonResponse(
            pairs,
            encoder=PairEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Pair.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(import_href=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Pair.objects.filter(id=id).update(**content)
        pairs = Pair.objects.get(id=id)
        return JsonResponse(
            pairs,
            encoder=PairEncoder,
            safe=False,
        )
