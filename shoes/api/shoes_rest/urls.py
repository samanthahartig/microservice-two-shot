from django.urls import path

from .views import list_pairs, shoe_pair

urlpatterns = [
    path("pairs/", list_pairs, name="list_pairs"),
    path("pairs/<int:id>/", shoe_pair, name="shoe_pair"),
]
