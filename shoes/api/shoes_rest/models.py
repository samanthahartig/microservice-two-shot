from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Pair(models.Model):
    manufacturer = models.TextField()
    name = models.TextField()
    color = models.TextField()
    photo_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="pair",
        on_delete=models.CASCADE,
        null=True,
    )

    def get_api_url(self):
        return reverse("shoe_pair", kwargs={"id": self.id})

    def __str__(self) -> str:
        return self.model_name
